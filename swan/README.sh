#!/bin/bash
#get SWAN Dockerfile
wget https://charade@bitbucket.org/charade/swan/raw/master/Dockerfile
#build local SWAN container
docker build --no-cache .
#label local SWAN container as xlib/swan
