#NOTE: developer tested the commands by folloing actions:
#      docker pull ubuntu:latest
#      docker images
#      docker run --memory=2g -i -t ubuntu:latest /bin/bash
#      docker run --memory=2g -i -t id /bin/bash      
#      docker-machine scp Dockerfile main:~/zoomx
#      docker build --memory=2g ~/zoomx
#      docker start --memory=2g 301176b69086
#      docker exec -it 301176b69086 /bin/bash
#NOTE: every line is a command, inline commenting is not supported by docker, always use new line for comment
#NOTE: merging RUN as file become stable as every RUN creates a commit which has a limit
#NOTE: xlibbox:zero is ground zero for all app containers
#NOTE: xlibbox:zero has basic ubuntu development env, R and python installed
#NOTE: see Dockerfile.zero for details
FROM charade/xlibbox:zero
ENV PATH "$PATH:$HOME/setup/THetA/bin"

### install and test lumpy
#$HOME is root
#clone everything to lumpy-sv
#
RUN apt-get update -y && apt-get upgrade -y
RUN apt-get install -y default-jdk
RUN apt-get install -y ant
RUN apt-get install -y python-matplotlib
RUN apt-get remove -y python-scipy --purge
pip install --upgrade pip
pip install cython
pip install bnpy

RUN cd $HOME/setup && git clone https://github.com/raphael-group/THetA.git
RUN cd $HOME/setup/THetA && ./install

#see https://github.com/raphael-group/THetA/blob/master/doc/MANUAL.txt
#for how to use theta2
#1. bin/RunTHetA interval_count_file [Options]
#  * bin/RunTHetA interval_count_file --TUMOR_FILE tumor_SNP_file --NORMAL_FILE normal_SNP_file [Options]
#results files: the set of all maximum likelihood solutions found.
#	  Each solution will be a single line in this file with fields:
#		1. NLL (Double) - solution neg. log likelihood (w/o constants)
#		2. mu  (Double,Double) - comma delimited list:  %normal, %tumor
#		3. C_2 (Integer: ... :Integer) - colon delimited list of
#		   inferred tumor interval copy numbers.  When multiple tumor
#		   populations are inferred this list is in the form
#		   (Integer,Integer: ... : Integer, Integer).
#		4. p* (Double, ... ,Double) - comma delimted list of inferred
#		   multinomial parameter \widehat{C\mu}
#		The chosen solution will be in a file with postfix ".BEST.results".
#		THetA will also output the individual solutions for n=2 and n=3 in
#		in files ending in ".n3.results" and ".n3.results" respectively
#	* results plots: PDFs that visualize the results will automatically be created.
#	* bounds files: a copy of the interval_count_file with additional
#	  columns for the upper and lower bounds used for the copy number
#				  estimate of each genomic interval for n=2 and n=3 (see example for details).
