Dockerfiles for Chrlie Xia's Programs
======================================

TODO
--------------------------------------
need to rebase xlib in all Dockerfiles to /home/xlib by default
or provided by --build-arg XLIB_TARGET=/home/xlib

charade/xlibbox:zero (Docker Hub)
--------------------------------------

= ubuntu:xenial

+ gcc/g++
+ git
+ vim
+ wget
+ Python
+ R

charade/xlibbox:basic (Docker Hub)
--------------------------------------

:zero 

+ samtools 
+ bedtools
+ various Python bio modules
+ various R bio modules

charade/xlibbox:swan (Docker Hub)
--------------------------------------

:basic

+ swan

charade/xlibbox:zoomx (Docker Hub)
--------------------------------------

:basic

+ zoomx

charade/xlibbox:svengine (Docker Hub)
--------------------------------------

:basic

+ svengine

charade/xlibbox:elsa (Docker Hub)
--------------------------------------

:basic

+ elsa

charade/xlibbox:grammy (Docker Hub)
--------------------------------------

:basic

+ grammy
