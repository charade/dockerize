#NOTE: developer tested the commands by folloing actions:
#      docker pull ubuntu:latest
#      docker images
#      docker run --memory=2g -i -t ubuntu:latest /bin/bash
#      docker run --memory=2g -i -t id /bin/bash      
#      docker-machine scp Dockerfile main:~/zoomx
#      docker build --memory=2g ~/zoomx
#      docker start --memory=2g 301176b69086
#      docker exec -it 301176b69086 /bin/bash
#NOTE: merging RUN as file become stable as every RUN creates a commit which has a limit

#NOTE: This Dockerfile adds stable and commonly used R, Python and C++ modules to xlibbox:zero
#NOTE: docker build . -t xlibbox:basic --build-arg XLIB_TARGET=/home/xlib
FROM charade/xlibbox:zero
MAINTAINER Charlie Xia <xia.stanford@gmail.com>

### setup environments ###
#ENV XLIB_HOME "/home/xlibbox"
#    -- this is expected to be set by docker build --build-arg XLIB_TARGET=/home/xlib
ENV TERM linux
ARG XLIB_TARGET=/home/xlib
ENV XLIB_HOME=$XLIB_TARGET
ENV XLIB_SETUP=$XLIB_HOME/setup
RUN ulimit -s unlimite \
        && dmkdir -p $XLIB_HOME \
        && mkdir $XLIB_SETUP \
WORKDIR $XLIB_SETUP

### install python packages ###
RUN pip install -U pip \
  && pip install -U --ignore-installed numpy scipy tables six pandas pysam pybedtools dendropy biopython

### install bwa ###
RUN git clone https://github.com/lh3/bwa.git \
  && cd bwa \
  && make
ENV PATH="$PATH:$XLIB_SETUP/bwa"

### install bedtools ###
RUN git clone https://github.com/arq5x/bedtools.git \
  && cd bedtools \
  && make
ENV PATH="$PATH:$XLIB_SETUP/bedtools/bin"

### install samtools ###
RUN git clone https://github.com/samtools/samtools.git \
  && git clone https://github.com/samtools/htslib.git \
  && cd samtools \
  && make
ENV PATH="$PATH:$XLIB_SETUP/setup/samtools"

### install R packages ###
RUN mkdir -p $(Rscript -e 'cat(Sys.getenv("R_LIBS_USER"))')
#Bioconductor: 
#  GenomicRanges, ggbio, GenomeInfoDb, BSgenome.Hsapiens.UCSC.hg19, BSgenome.Hsapiens.UCSC.hg38, Biostrings
RUN Rscript -e 'source("http://bioconductor.org/biocLite.R"); biocLite("BSgenome.Hsapiens.UCSC.hg19",ask=F)'
RUN Rscript -e 'source("http://bioconductor.org/biocLite.R"); biocLite("BSgenome.Hsapiens.UCSC.hg38",ask=F)'
RUN Rscript -e 'source("http://bioconductor.org/biocLite.R"); biocLite("GenomicRanges",ask=F)'
RUN Rscript -e 'source("http://bioconductor.org/biocLite.R"); biocLite("GenomeInfoDb",ask=F)'
RUN Rscript -e 'source("http://bioconductor.org/biocLite.R"); biocLite("Biostrings",ask=F)'
RUN Rscript -e 'source("http://bioconductor.org/biocLite.R"); biocLite("Rsamtools",ask=F)'
RUN Rscript -e 'source("http://bioconductor.org/biocLite.R"); biocLite("biovizBase",ask=F)'
RUN Rscript -e 'source("http://bioconductor.org/biocLite.R"); biocLite("ensembldb",ask=F)'
RUN Rscript -e 'source("http://bioconductor.org/biocLite.R"); biocLite("ggbio",ask=F)'
RUN Rscript -e 'source("http://bioconductor.org/biocLite.R"); biocLite("Biobase",ask=F)'
RUN Rscript -e 'source("http://bioconductor.org/biocLite.R"); biocLite("BSgenome",ask=F)'
RUN Rscript -e 'source("http://bioconductor.org/biocLite.R"); biocLite("S4Vectors",ask=F)'
#CRAN: 
#  optparse, rjson, RColorBrewer, devtools, igraph, cowplot, ggplot2
RUN Rscript -e 'install.packages("optparse",repos="http://cran.us.r-project.org")'  
RUN Rscript -e 'install.packages("igraph",repos="http://cran.us.r-project.org")'  
RUN Rscript -e 'install.packages("rjson",repos="http://cran.us.r-project.org")'  
RUN Rscript -e 'install.packages("ggplot2",repos="http://cran.us.r-project.org")'  
RUN Rscript -e 'install.packages("RColorBrewer",repos="http://cran.us.r-project.org")'  
RUN Rscript -e 'install.packages("cowplot",repos="http://cran.us.r-project.org")'  
RUN Rscript -e 'install.packages("devtools",repos="http://cran.us.r-project.org")'  
RUN Rscript -e 'install.packages("BH",repos="http://cran.us.r-project.org")'  
RUN Rscript -e 'install.packages("data.table",repos="http://cran.us.r-project.org")'  
RUN Rscript -e 'install.packages("digest",repos="http://cran.us.r-project.org")'  
RUN Rscript -e 'install.packages("hash",repos="http://cran.us.r-project.org")'  
RUN Rscript -e 'install.packages("methods",repos="http://cran.us.r-project.org")'  
RUN Rscript -e 'install.packages("parallel",repos="http://cran.us.r-project.org")'  
RUN Rscript -e 'install.packages("plyr",repos="http://cran.us.r-project.org")'  
RUN Rscript -e 'install.packages("robustbase",repos="http://cran.us.r-project.org")'  
RUN Rscript -e 'install.packages("sets",repos="http://cran.us.r-project.org")'  
RUN Rscript -e 'install.packages("stringr",repos="http://cran.us.r-project.org")'  
RUN Rscript -e 'install.packages("zoo",repos="http://cran.us.r-project.org")'  
RUN Rscript -e 'install.packages("Rcpp",repos="http://cran.us.r-project.org",type="source")'  
RUN Rscript -e 'install.packages("RcppArmadillo",repos="http://cran.us.r-project.org",type="source")'  
RUN Rscript -e 'install.packages("vcfR",repos="http://cran.us.r-project.org",type="source")'  

### make sure all paths ###
RUN env

### mount your server dir for data analysis
#docker run -d --name="foo" -v "/home/lixia:/home/lixia" ubuntu
