#!/bin/bash

umask -p 000          #for docker permission on nfs 
### Gistic Analysis
#sample selection done by Paul and my preprocessing in R
#see emails for details, the file is provided in 40_PatientStatistics/Data_Survival_Select.pid.txt
ln -s 40_PatientStatistics/Data_Survival_Select.pid.txt Charlie_QC_Remaining.txt

mkdir 22_GisticAnalysis
#combine into one giant seg file for gistic2
analysis_home="/mnt/ix2/Sandbox/charlie/shako/imh/crc_imh"
fname="Charlie_Gistic2_Input.nogene.cns.gistic2"
b="$analysis_home/22_GisticAnalysis"
o="$b/$fname" #merged gistic2 input file
q="Data_Survival_Paul.pid.txt"
rm -f $o
grep -f $q -l 10_RawData/*.gistic2 | wc -l #134 samples
echo -e "Sample\tChromosome\tStart\tEnd\tNum_Probes\tSegment_Mean" >$o
for f in `grep -f $q -l 10_RawData/*.gistic2`; do
  echo $f;
  tail -n +2 $f >>$o
done;
awk '{ print $1 }' $b/$fname | uniq | wc -l #n=135

#run gistic2 from docker
docker_image="charade/xlibbox:gistic2"
docker_opt="-t -v /mnt/ix2:/mnt/ix2"
analysis_home="/mnt/ix2/Sandbox/charlie/shako/imh/crc_imh"
fname="Charlie_Gistic2_Input.nogene.cns.gistic2"
b="$analysis_home/22_GisticAnalysis"
o="$b/$fname" #merged gistic2 input file
g="./refgenefiles/hg38.UCSC.add_miR.160920.refgene.mat"
cd $b
noiseValues=(0.1 0.2 0.3 0.4)
for noise in ${noiseValues[*]}; do
  d="noise=$noise"
  wkdir="$b/$d"
  mkdir $d
  cp "$b/$fname" $d
  cd $d
  gistic2_log=$wkdir/${fname}.log
  gistic2_opt="-b $wkdir -seg $wkdir/$fname -refgene $g -maxspace 10000 -ta $noise -td $noise -qvt 0.25 -broad 1 -brlen 0.7 -twoside 1 -conf 0.99 -genegistic 1 -armpeel 1 -savegene 1 -res 0.05 -smallmem 1 -js 4 -fname $fname"
  docker_cmd="docker run -d $docker_opt $docker_image /bin/sh -c 'echo gistic2 $gistic2_opt >$gistic2_log && cd /root/setup/gistic2 && ./gistic2 $gistic2_opt >>$gistic2_log 2>&1'"
  echo $docker_cmd >$fname.docker.run # nohup does not work with this file
  sh $fname.docker.run
  cd ..
done

#copy latest gistic2 data to R analysis input files
