docker build -t charade/xlibbox:gistic2 .
docker tag 88a99268b437 charade/xlibbox:gistic2      #must do interactively
## reuse image through export
docker export cid:26eb4cbcff2b -o gistic2.export.tar #export a container to image
docker import gistic2.export.tar                     #import an image, return image id, without tag
docker run -it iid:12478b3c4695 /bin/bash            #run an interactive shell on imported image   
docker rm $(docker ps -aq)                           #remove all container (associated with image)
docker rm rmi iid:12478b3c4695                       #safely removes image
## reuse image through save, slower than import, multiple layers
docker save iid:e294903e8026 -o gistic2.save.tar     #save an image to docker history
docker load -i gistic2.save.tar                      #load an image from docker history, return tag
docker run -it charade/xlibbox:gistic2 /bin/bash     #run an interactive shell on imported image
docker rm $(docker ps -aq)                           #remove all container (associated with image)
docker rmi charade/xlibbox:gistic2                   #safely removes image
